const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order')
const auth = require('../auth');


//Create an order
router.post('/', auth.verify, orderController.addOrder);

//retrieval all orders
router.get('/all', auth.verify,  orderController.getAllOrders);
	
// retrive authenticated user's orders
router.get('/', auth.verify, orderController.getUserOrders);


//Test an order
router.post('/testOrder', auth.verify, orderController.testOrder);


module.exports = router;



