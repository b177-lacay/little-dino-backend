const express = require('express');
const router = express.Router();
const dotenv = require("dotenv");

dotenv.config();
const stripe = require("stripe")(process.env.STRIPE_SECRET_TEST)

router.post("/payment", (req,res) => {
	stripe.charges.create({
		source: req.body.tokenId,
		amount: req.body.amount,
		currency: "php",	
	},(stripeErr, stripeRes) => {
		if(stripeErr){
			res.status(500).json(stripeErr);
		}else{
			res.status(200).json(stripeRes);
		}
	} )
});

module.exports = router;
