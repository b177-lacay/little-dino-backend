const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cart')
const auth = require('../auth');


//Create a cart
router.post('/', auth.verify, cartController.addCart);

// checkout
router.post('/checkout', auth.verify, cartController.addCheckout);

//retrieval all carts
router.get('/all', auth.verify,  cartController.getAllCarts);
	
// retrive authenticated user's carts
router.get('/', auth.verify, cartController.getUserCart);


// increment authenticated user's carts
router.put('/increment', auth.verify, cartController.incrementCart);


// decrement authenticated user's carts
router.put('/decrement', auth.verify, cartController.decrementCart);


// delete authenticated user's carts
router.delete('/delete', auth.verify, cartController.deleteCart);


// delete authenticated user's carts
router.put('/removeCartItem', auth.verify, cartController.removeCartItem);


module.exports = router;



