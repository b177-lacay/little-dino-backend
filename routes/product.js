const express = require('express');
const router = express.Router();
const productController = require('../controllers/product')
const auth = require('../auth');


//Create a product
router.post('/', auth.verify, productController.addProduct);


// retrive all available products
router.get('/', productController.getAllAvailableProducts);


//retrieval of available products
router.get('/search', productController.getAllAvailableByTags);


//get all products by admin
router.get('/all', auth.verify, productController.getAllProducts);


//retrieval of specific product

router.get('/:productId', productController.getProduct);



//update product information
router.put('/:productId', auth.verify, productController.updateProduct);



//archiving a product
router.post('/archive', auth.verify, productController.archiveProduct);


module.exports = router;



