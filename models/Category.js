const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 50,
    uppercase: true
  }
});

module.exports = mongoose.model('Category', categorySchema);

