const Product = require('../models/Product');
const Category = require('../models/Category');
const auth = require('../auth');
const validate = require('../models/validator');

// [SECTION- get categories]
exports.getAllCategories = async (req, res) => {
	try{
		
		const categories = await Category.find().sort('name');
  		res.send(categories);
	
	}catch(err){
		return res.status(500).send(err.message);
	}
};


// [SECTION- Create category]
exports.addCategory = async (req, res) => {

const { error } = validate.validateCategory(req.body); 
 
if (error) return res.status(400).send(error.details[0].message);

const data = auth.decode(req.headers.authorization)

if(!data.isAdmin) return res.send('Unauthorized user');

	try{

		let isNameTaken = await Category.findOne({"name": req.body.name})

	 	if(isNameTaken) return res.send('Category name is already in use.')

		let category = new Category({
				name: req.body.name
				})
  		category = await category.save();
  
  		return res.send(category);

	}catch(err){
		return res.status(500).send(err.message);
	}
};


// [SECTION- Delete category]
exports.deleteCategory = async (req, res) => {
	try{
  		const category = await Category.findByIdAndRemove(req.params.id);

  		if (!category) return res.status(400).send('Invalid categoryId.');

 		 return res.send(category);

	}catch(err){
	return res.status(500).send(err.message);
	}
};


// [SECTION- Get category]
exports.getCategory = async (req, res) => {

	try{

		const category = await Category.findById(req.params.id);

  		if (!category) return res.status(404).send('The category with the given ID was not found.');

  		res.send(category);
	
	}catch(err){
		res.status(500).send(err.message);
	}

};

// [SECTION- get prud by catergory]
exports.getProductsByCategory = (req,res) => {

return Category.findById(req.params.categoryId)

			   .then(category => {

			   	if (!category) return res.status(400).send('Invalid category!')

			   		console.log(category._id)

			   	return Product.find()

							  .and([{"category.categoryId": category._id},{isAvailable: true}])

			   		.then(product => {
			   			// console.log(product)

			   		if(!product) return res.status(400).send('Invalid product!')

			   		return res.send(product)	
			   		})

			   })

			   .catch(err => res.status(500).send(err.message));
			   
}








